package com.greatlearning.restaurant.aop;

import java.util.Date;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import com.greatlearning.restaurant.entity.AuditLog;
import com.greatlearning.restaurant.repository.AuditRepository;

@Configuration
@Aspect
public class AspectConfig {

	@Autowired
	AuditRepository auditRepository;
	
	@Around("execution(public * com.greatlearning.restaurant.service.impl.*.*(..))")
	public Object logBeforeAndAfterAllMethods(ProceedingJoinPoint proceedingJoinPoint) throws Throwable{
		AuditLog startLog = new AuditLog();
		startLog.setCreateDate(new Date());
		startLog.setDiscription(proceedingJoinPoint.getSignature().getName() + " Started");
		auditRepository.saveAndFlush(startLog);

		Object result = proceedingJoinPoint.proceed();
		AuditLog endLog = new AuditLog();
		endLog.setCreateDate(new Date());
		endLog.setDiscription(proceedingJoinPoint.getSignature().getName() + " Ended");
		auditRepository.saveAndFlush(endLog);
		return result;
	}
	
}
