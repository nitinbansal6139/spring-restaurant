package com.greatlearning.restaurant.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.restaurant.entity.FinancialReport;
import com.greatlearning.restaurant.entity.User;
import com.greatlearning.restaurant.service.RestaurantItemService;
import com.greatlearning.restaurant.service.UserService;

@RestController
@RequestMapping("/admin")
public class AdminController {

	@Autowired
	RestaurantItemService restaurantItemService;
	
	@Autowired
	UserService userService;
	
	@GetMapping("/users/add")
	public String addUser(User user) {
		userService.createUser(user);
		return "User created with role "+user.getRole();
	}
	
	@GetMapping("/users/update")
	public String updateUser(User user) {
		String message = userService.updateUser(user);
		return message;
	}
	
	@GetMapping("/users/fetch")
	public User fetchUser(int id) {
		return userService.fetchUser(id);
	}
	
	@GetMapping("/users/delete")
	public String deleteUser(int id) {
		String message = userService.deleteUser(id);
		if(message == null)
			return "User not found with id "+id;
		return "User deleted";
	}
	
	@GetMapping("/viewDailyBillings")
	public List<FinancialReport> getDailyReport() {
		return restaurantItemService.viewDailyReport();
	}

	@GetMapping("/viewMonthlyBillings")
	public List<FinancialReport> getMonthlyReport() {
		return restaurantItemService.totalMonthlySale();
	}
}
