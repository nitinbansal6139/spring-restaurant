package com.greatlearning.restaurant.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.greatlearning.restaurant.entity.Item;

@Repository
public interface ItemDetailsRepository extends JpaRepository<Item, Integer>{

}
